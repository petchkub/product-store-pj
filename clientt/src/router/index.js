import { createRouter, createWebHistory } from 'vue-router'
import ProductView from '@/views/ProductView.vue'
import CategoryView from '@/views/CatagoryView.vue';


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Product',
      component: ProductView
    },
    {
      path: '/categories',
      name: 'CategoryList',
      component: CategoryView
    }
  ]
})

export default router
